A 3D WebGL game written in the Python fork "GDscript" using the Godot game engine.

The game is playable at: https://consistency.itch.io/one-way-ticket-ep-1

_________________________________________________________________________

Controls:

WASD - Move player

Space - Jump

Shift - run

___________________________________________________________________________

This game was made for the Two-Minute Horror Jam and may end up being a continuing series. It's inspired by episodic mystery/horror series such as the Twilight Zone, however the story behind this first episode isn't all too serious.

This episode revolves around an evening shift manager closing a fast-food chicken restaurant by himself.

There are 2 endings, a true ending and a false ending. You will get the false ending if you're too slow.

All that aside, please enjoy!

Music:

Kraftwerk - The Hall of Mirrors

FesliyanStudios.com - Fear