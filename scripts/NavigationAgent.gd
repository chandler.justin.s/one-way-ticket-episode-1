extends KinematicBody

# Implementation of A* algorithm for chicken AI

var path = []
var path_ind = 0

var velocity = Vector3()
var finalLoc = Vector3()

var Done = false
var ShouldChase = false

var targetT = Transform()
onready var selfT = self.get_transform()
onready var meshT = self.get_child(0).get_transform()
onready var meshN = self.get_child(0)

const SPEED = 5.0
const ACCEL = 5.0
const GRAVITY = -9.8 * 2

onready var nav = self.get_parent()

func _ready():
	pass

func _physics_process(delta):
	if path.size() != 0 and Global.Player != null:
		if (path[path.size()-1] - Global.Player.get_transform().origin).length() > 1 and ShouldChase:
			move_to(Global.Player.get_transform().origin)
	if path_ind < path.size():
		
		var direction = (path[path_ind] - self.get_transform().origin)
		if direction.length() <= 1:
			path_ind += 1
			
		else:
			direction.y = 0
			direction = direction.normalized()
			velocity = velocity.linear_interpolate(direction * SPEED, delta * ACCEL)
			velocity = self.move_and_slide(velocity, Vector3(0,1,0))
			if not self.is_on_floor():
				velocity.y += GRAVITY * delta
			finalLoc = direction
			
			meshT = meshN.get_transform()
			targetT = meshT.looking_at(direction, Vector3(0, 1, 0))
			var newRot = Quat(meshT.basis).slerp(targetT.basis, delta*ACCEL)
			meshN.set_transform(Transform(newRot,meshT.origin))
	else:
		velocity = velocity.linear_interpolate(Vector3(0,velocity.y,0), delta*ACCEL)
		velocity = self.move_and_slide(velocity, Vector3(0,1,0))
		velocity.y += GRAVITY * delta
		
		if ShouldChase:
			meshT = meshN.get_transform()
			targetT = meshT.looking_at(Global.Player.get_transform().origin - self.get_transform().origin, Vector3(0, 1, 0))
			var newRot = Quat(meshT.basis).slerp(targetT.basis, delta*ACCEL)
			meshN.set_transform(Transform(newRot,meshT.origin))
		CheckDone()

func move_to(target_pos):
	path = nav.get_simple_path(self.get_transform().origin, target_pos)
	path_ind = 0

func RunAway():
	move_to(self.get_parent().get_node("TargetRun").get_transform().origin)
	Done = true

func ChasePlayer():
	move_to(Global.Player.get_transform().origin)
	ShouldChase = true

func CheckDone():
	if Done:
		self.get_parent().remove_child(self)
