extends Node

var Player
var CurrScene
var UIReference
var ObjectiveReference
var CounterReference
var DrawerReference
var ChickenRunReference
var ChickenRunAreaReference
var BucketEmptyReference
var BucketFilledReference
var ChaseChickenReference

var RegisterCount = 0

var Phase = "Garbage"

func _ready():
	print(self.get_tree().current_scene.name)
	CurrScene = self.get_tree().current_scene.name
	if CurrScene == "MainScene":
		Player = self.get_tree().current_scene.get_node("Player")

func reload():
	print(self.get_tree().current_scene.name)
	CurrScene = self.get_tree().current_scene.name
	if CurrScene == "MainScene":
		Player = self.get_tree().current_scene.get_node("Player")
		UIReference = get_tree().current_scene.get_node("ObjectivesUI")
		ObjectiveReference = UIReference.get_node("ColorRect/ColorRect/ObjectiveText")
		CounterReference = UIReference.get_node("ColorRect/ColorRect/CounterText")
		DrawerReference = self.get_tree().current_scene.get_node("Player/Spatial/ClippedCamera/drawer_carry")
		ChickenRunReference = self.get_tree().current_scene.get_node("WindowNavigation/ChickenRunNPC")
		ChickenRunAreaReference = self.get_tree().current_scene.get_node("ChickenRunArea")
		BucketEmptyReference = Player.get_node("mop_bucket_empty")
		BucketFilledReference = Player.get_node("mop_bucket_filled")
		ChaseChickenReference = self.get_tree().current_scene.get_node("Navigation/ChaseChicken")

func SwitchRegisterPhase():
	Phase = "Register"
	ObjectiveReference.bbcode_text = "Objective:\nPut the register drawers in the safe"
	CounterReference.bbcode_text = "[right]\n"+str(RegisterCount)+"/3"

func SwitchMopPhase():
	if ChickenRunReference != null:
		ChickenRunReference.queue_free()
		ChickenRunAreaReference.queue_free()
	Phase = "Mop"
	ObjectiveReference.bbcode_text = "Objective:\nFill the mop bucket and mop the kitchen"
	CounterReference.bbcode_text = ""

func SwitchInvestPhase():
	Phase = "Investigate"
	ChaseChickenReference.set_translation(Vector3(-8.998,-9.225,-4.957))
	self.get_tree().current_scene.get_node("BoxFallPlayer").play()
	ObjectiveReference.bbcode_text = "Objective:\nInvestigate the noise from the basement"

func SwitchChasePhase():
	Phase = "Chase"
	self.get_tree().current_scene.get_node("horror_player").stop()
	self.get_tree().current_scene.get_node("FearPlayer").play()
	ObjectiveReference.bbcode_text = "Objective:\nESCAPE"
	ChaseChickenReference.ChasePlayer()

func AddRegister():
	RegisterCount += 1
	if RegisterCount == 2:
		ChickenRunAreaReference.Activate()
	CounterReference.bbcode_text = "[right]\n"+str(RegisterCount)+"/3"
	if RegisterCount == 3:
		SwitchMopPhase()

