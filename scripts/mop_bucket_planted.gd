extends StaticBody

var Taken = false
var Slipped = false

func _ready():
	pass

func _process(delta):
	if Slipped and self.get_parent().get_translation().x < -12.71:
		self.get_parent().translate(Vector3(0, 0, -14*delta))
		self.get_tree().current_scene.Ending = true
		self.get_tree().current_scene.Modifier = 10.0
	elif Slipped and self.get_parent().get_translation().x >= -12.71 and self.get_parent().get_translation().x < 30.0:
		self.get_tree().current_scene.SetSwitchTimer()
		self.get_parent().translate(Vector3(0, 0, -14*delta))

func _on_bucket_area_body_entered(body):
	if Global.Phase == "Mop" and body == Global.Player and not Taken:
		Taken = true
		self.get_node("CollisionShape").disabled = true
		self.get_parent().get_node("empty").visible = false
		Global.BucketEmptyReference.visible = true
	
	if Global.Phase == "Mop" and body == Global.Player and Taken and Global.BucketFilledReference.visible:
		Global.BucketFilledReference.visible = false
		self.get_node("CollisionShape").disabled = false
		self.get_parent().get_node("filled").visible = true
		Global.SwitchInvestPhase()

	if Global.Phase == "Chase" and body == Global.Player:
		Global.ChaseChickenReference.queue_free()
		self.get_node("Camera").current = true
		self.get_tree().current_scene.get_node("FearPlayer").stop()
		Global.Player.get_node("Spatial/ClippedCamera").InputDisabled = true
		Slipped = true
		self.get_node("SizzlePlayer").play()
