extends Spatial

var processing = true

var collected = 0

var test : AudioStreamPlayer

onready var PlayerReference = get_tree().current_scene.get_node("Player")
onready var UIReference = get_tree().current_scene.get_node("ObjectivesUI")
onready var ObjectiveReference = UIReference.get_node("ColorRect/ColorRect/ObjectiveText")
onready var CounterReference = UIReference.get_node("ColorRect/ColorRect/CounterText")

func _ready():
	pass

func Dumped(body):
	if body == PlayerReference and PlayerReference.get_node("Spatial/ClippedCamera/trashbag_carry").visible:
		PlayerReference.get_node("Spatial/ClippedCamera/trashbag_carry").visible = false
		CounterReference.bbcode_text = "\n[right]" + str(collected) + "/3"
		get_tree().current_scene.get_node("DumpedSound").play()
		if collected != 3:
			processing = true
		else:
			PlayerReference.get_node("AudioStreamPlayer3D").stop()
			PlayerReference.get_node("AudioStreamPlayer3D/LoopPlayer").stop()
			self.get_tree().current_scene.get_node("horror_player").play()
			self.get_tree().current_scene.get_node("chicken_idle2").activated = true
			Global.SwitchRegisterPhase()
