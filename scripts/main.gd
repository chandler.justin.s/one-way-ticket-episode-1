extends Spatial

var this_env : Environment
var timer = 0.0

var ShouldFadeOut = false
var PrepareFadeOut = false
var Ending = false
var PrepTimer = 0
var Prepped = false
var SwitchTimer = 0
var Modifier = 1.0

func _ready():
	this_env = load("res://default_env.tres")
	this_env.adjustment_brightness = 0.00
	Global.reload()
	Global.Phase = "Garbage"
	Global.RegisterCount = 0

func SetPrepareFadeout():
	PrepareFadeOut = true
	PrepTimer = OS.get_ticks_msec() + 2000

func SetSwitchTimer():
	ShouldFadeOut = true
	SwitchTimer = OS.get_ticks_msec() + 5000

func _process(delta):
	timer += delta
	if(timer > 1 and timer < 3):
		self.visible = false
		$Player/Spatial/ClippedCamera/trashbag_carry.visible = false
		Global.DrawerReference.visible = false
		Global.BucketEmptyReference.visible = false
		Global.BucketFilledReference.visible = false
	if this_env.adjustment_brightness < 1.0 and timer > 3 and not ShouldFadeOut:
		self.visible = true
		$Player/Spatial/ClippedCamera.isready = true
		this_env.adjustment_brightness = this_env.adjustment_brightness + (1 - this_env.adjustment_brightness) * delta * 2.0

	if PrepareFadeOut and not Prepped:
		if OS.get_ticks_msec() > PrepTimer:
			Prepped = true
			SetSwitchTimer()

	if ShouldFadeOut:
		this_env.adjustment_brightness = this_env.adjustment_brightness + (0 - this_env.adjustment_brightness) * delta * 2.0 * Modifier
		if OS.get_ticks_msec() > SwitchTimer and not Ending:
			self.get_tree().change_scene("res://FalseEnd.tscn")
		elif OS.get_ticks_msec() > SwitchTimer and Ending:
			self.get_tree().change_scene("res://TrueEnd.tscn")
